package databaseoperations;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

public class InsertDataIntoDataBase {
	/*
	 * helper method to create the table
	 */
	public void createTable(String tablename) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni","root","system");
				Statement stmt = conn.createStatement();) {

			stmt.executeUpdate(tablename);
			System.out.println("Created table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	/*
	 * helper method to insert data into the database
	 */

	public void insertData(String adddata) {
		try (Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
				Statement stmt = conn.createStatement();) {

			stmt.executeUpdate(adddata);
			System.out.println("data inserted table in given database...");
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
}

