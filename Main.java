package databaseoperations;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Main {
 public static void main(String args[]) {
	 //creating database class object
	 DataBase db=new DataBase();
	 //to call the method in database object
	 try {
		db.readJson();
	} catch (IOException e) {
		e.printStackTrace();
	}
 
	 System.out.println("************MYSQL DATABASE ************");
		System.out.println("1.Create the table");
		System.out.println("2.Insert data into the table");
		Scanner sc = new Scanner(System.in);
		System.out.println("enter your choice");
		int ch = sc.nextInt();
		switch (ch) {
		case 1:
			/*
			 * call a method to create the table in database
			 */
			String tablename = "CREATE TABLE newjoinees1 "+ "(USERNAME VARCHAR(255) , " +"ID VARCHAR(255) ,"+ "OTP VARCHAR(255),"+"RECOVERY_CODE VARCHAR(255),"+"Emp_firstname VARCHAR(255),"
					+ " Emp_lastname VARCHAR(255), " +"DEPARTMENT VARCHAR(255)," +" LOCATION VARCHAR(255));";
			System.out.println(tablename);
			InsertDataIntoDataBase createtabelobj = new InsertDataIntoDataBase();
			createtabelobj.createTable(tablename);
			break;
		case 2:
			/*
			 *call a method to insert data into database 
			 */
			 //Creating a JSONParser object
		      JSONParser jsonParser = new JSONParser();
		      try {
		         //Parsing the contents of the JSON file
		         JSONObject jsonObject = (JSONObject) jsonParser.parse(new FileReader("C:\\Users\\tchavala\\eclipse-workspace2\\Test1\\src\\new_joiners"));
		         //Retrieving the array
		         JSONArray jsonArray = (JSONArray) jsonObject.get("details");
		         Connection con = DriverManager.getConnection("jdbc:mysql://localhost:3306/triveni", "root", "system");
		         //Insert a row into the newjoinees table
		         PreparedStatement pstmt = con.prepareStatement("INSERT INTO newjoinees1 values (?, ?, ?, ?, ?, ?,?,? )");
		          for(Object object : jsonArray) {
		            JSONObject record = (JSONObject) object;
		            String username = (String) record.get("USERNAME");
		            String id=(String) record.get("ID");
		            String otp = (String) record.get("OTP");
		            String recovery_code = (String) record.get("RECOVERY_CODE");
		            String firstname = (String) record.get("FIRSTNAME");
		            String lastname = (String) record.get("LASTNAME");
		            String department = (String) record.get("DEPARTMENT");
		            String location=(String)record.get("LOCATION");
					pstmt.setString(1, username);
		            pstmt.setString(2,id);
		            pstmt.setString(3,otp);
		            pstmt.setString(4,recovery_code);
		            pstmt.setString(5,firstname);
		            pstmt.setString(6,lastname);
		            pstmt.setString(7,department);
		            pstmt.setString(8,location);
		            pstmt.executeUpdate();
		         }  
		         System.out.println("Records inserted.....");
		      }
		   catch (FileNotFoundException e) {
	         e.printStackTrace();
	      } catch (IOException e) {
	         e.printStackTrace();
	      } catch (ParseException e) {
	         e.printStackTrace();
	      } catch (Exception e) {
	         // TODO Auto-generated catch block
	         e.printStackTrace();
	      }
	   }
	}
}
 
